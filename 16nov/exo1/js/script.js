// Créer une variable
var str = "123456789";
var add = "987654321";

console.log({ str, add })

// Additionner deux chaines de caractères = concaténation
var result = str + add;
console.log({ result })


// Connaitre le type d'une variable
var strType = typeof str;

// Convertir une chaine de caractère en entier/flottant
var convertStrToNumber = +str;
var convertAddToNumber = +add;
var convertedResult = convertStrToNumber + convertAddToNumber;

console.log({ convertStrToNumber, convertAddToNumber, convertedResult })

// La conversion ne fonctionne qu'avec des entiers/flottants
var firstName = "Julien";
var convertedFirstname = +firstName
console.log({ convertedFirstname })