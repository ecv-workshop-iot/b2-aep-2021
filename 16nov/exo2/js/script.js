/* 
Une chaine de caratcères et comprise comme un tableau de caractères par votre ordinateur
*/
let idx = 0;
let str = "123456789";
let result = 0;
let strResult = '';



/* 
Faire une boucle sur la chaine de caractères
*/
for(let item of str){
    // Ajouter 1 à idx
    idx++;

    // Vérifier la valeur de l'index
    if(idx < str.length === true){
        // Concaténer strResult et item + ajouter "+"
        strResult += item + '+';
    }
    else{
        // Concaténer strResult et item + ajouter "="
        strResult += item + '=';
    }

    // Convertir la chaine de caractère en entier
    let convert = +item;

    // Additionner result et la valeur de l'item
    result += convert
}
// Concaténer le résultat dans strResult
strResult += result;

console.log( 'strResult', typeof strResult );
console.log( 'result', typeof result );
console.log(strResult);