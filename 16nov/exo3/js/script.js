/* 
Create variable to bind HTML markup: form and input
*/
    var myForm = document.querySelector('.form-email');
    var userEmail = document.querySelector('#user-email');
//

/* 
Bind 'submit' event on the form
*/
    myForm.addEventListener('submit', function(event){
        // Stop event
        event.preventDefault();
        
        // Dislay only Google email
        if( userEmail.value.indexOf('@gmail.') !== -1 ){
            // Display input value in the console
            console.log('submit', userEmail.value);
        }
    })
//