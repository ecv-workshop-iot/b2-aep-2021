/* 
Create variable to bind HTML markup: form and input
*/
    var sectionLogin = document.querySelector('#section-login');
    var sectionLogged = document.querySelector('#section-logged');

    var myForm = document.querySelector('.form-email');
    var userEmail = document.querySelector('#user-email');
    var userPassword = document.querySelector('#user-password');
    var formIsValid = undefined;
//

/* 
Bind 'submit' event on the form
*/
    myForm.addEventListener('submit', function(event){
        // Stop event
        event.preventDefault();

        // Change form validation
        formIsValid = true;

        // Une adresse email valide
        if( !userEmail.value.includes('@') || !userEmail.value.includes('.') ){ formIsValid = false }

        // Minimum 5 caractère pour le mot de passe
        if( userPassword.value.length < 5 ){ formIsValid = false }
        else{
            // Vérifier le mot de passe "poiuytreza"
            if( userPassword.value !== "poiuytreza" ){ formIsValid = false }
        }

        // Validation final du formulaire
        if(formIsValid){
            console.log( 'Password OK', userPassword.value, userPassword.value.length )
            console.log( 'Email OK', userEmail.value )
            
            // Stocker l'email en localStorage
            localStorage.setItem('user', userEmail.value);

            // Update view
            sectionLogin.classList.remove('active')
            sectionLogged.classList.add('active')
        }
        else{
            console.log( "Le formulaire n'est pas valide" )
        }
    })
//

/* 
Get local storage value
*/
    var localEmail = localStorage.getItem('user');
    if(localEmail !== null){
       // Add classe 'active' to sectionLogged
       sectionLogged.classList.add('active')
    }
    else{
        // Add classe 'active' to sectionLogin
        sectionLogin.classList.add('active')
    }
//