/* 
- Diplome
- networks
- bio
- Geolocalisation
*/

var identity = {
    "@context": "https://schema.org",
    "@type": "Person",
    "workLocation": {
        "@type": "Place",
        "address": {
	        "addressLocality": "Aix-en-Provence",
        	  "addressRegion": "PACA",
	          "postalCode": "13100",
    	      "streetAddress": "21, boulevard de la République"
        },
        "geo": {
            "@type": "GeoCoordinates",
            "latitude": "40.75",
            "longitude": "73.98"
        }
    },
    "colleague": [
        "https://www.linkedin.com/in/damient75/",
        "https://www.linkedin.com/in/jonathan-c-97bb5183/",
        "https://www.linkedin.com/in/camillerobertdesmoulieres/"
    ],
    "email": "mailto:julien@dwsapp.io",
    "image": "./img/profil.jpeg",
    "jobTitle": "Freelancer",
    "name": "Julien Jacques Noyer",
    "telephone": "0611546016",
    "url": "https://dwsapp.io",
    "knowsAbout": "Lorem ipsum dolor sit, amet consectetur adipisicing elit. Quisquam et nesciunt aliquid accusamus a vel fuga, fugiat similique quam deleniti cum, minima hic ducimus blanditiis at, magnam officia. Voluptatibus, in.",
    "publishingPrinciples": "https://dwsapp.io"
}